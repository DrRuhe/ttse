#![allow(unused)]

mod structs;

use crate::structs::{Cached, Entries, Entry};

extern crate skim;
use colored::Colorize;
use cursive::utils::markup::*;
use cursive::{theme::*, Cursive};

use cursive::view::IntoBoxedView;
use fuzzy_matcher::skim::SkimMatcherV2;
use fuzzy_searchers::framework::{
    FuzzySearchResult, FuzzySearcher, MatchFormatter, StatefulFuzzySearcher,
};
use skim::prelude::*;
use std::fmt::Display;
use std::ops::Range;
use std::{io::Cursor, mem::Discriminant};
use structs::EntryFields;

use clap::Parser;
use directories::ProjectDirs;
use tldr_parsing::structs::{Language, Platform};

use cursive::traits::*;
use cursive::views::*;

/// Simple program to greet a person
#[derive(Parser, Debug)]
#[clap(about, version, author)]
struct Args {
    /// Search Query
    #[clap()]
    query: Vec<String>,
    /// the language of the query and the search results
    #[clap(short, long, default_value_t = Language::en)]
    language: Language,
    // the platform for which results should be displayed
    #[clap(short, long)]
    platform: Option<Platform>,
    #[clap(short, long)]
    non_interactive: bool,
}

#[derive(Debug)]
struct MatchedEntry {
    entry: FuzzySearchResult<Entry>,
}
impl From<FuzzySearchResult<Entry>> for MatchedEntry {
    fn from(entry: FuzzySearchResult<Entry>) -> Self {
        MatchedEntry { entry }
    }
}

impl Display for MatchedEntry {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let format_matched: MatchFormatter = |str, f| write!(f, "{}", str.red());
        let format_unmatched: MatchFormatter = |str, f| write!(f, "{}", str);

        self.entry.format_field(
            &EntryFields::CommandName,
            format_matched,
            format_unmatched,
            f,
        )?;
        writeln!(f, "");
        self.entry.format_field(
            &EntryFields::Description,
            format_matched,
            format_unmatched,
            f,
        )?;
        writeln!(f, "");
        self.entry.format_field(
            &EntryFields::CommandStr,
            format_matched,
            format_unmatched,
            f,
        )?;
        writeln!(f, "");

        Ok(())
    }
}

impl IntoBoxedView for MatchedEntry {
    fn into_boxed_view(self) -> Box<dyn cursive::View> {
        fn create_styled_string(entry: &MatchedEntry, field: EntryFields) -> StyledString {
            let style = Style {
                effects: Effect::Simple.into(),
                color: ColorStyle::secondary(),
            };

            let format_matched = Box::new(move |str: &str, mut spanned_string: StyledString| {
                spanned_string.append_styled(str, style.clone());
                return spanned_string;
            });
            let format_unmatched = Box::new(|str: &str, mut spanned_string: StyledString| {
                spanned_string.append_plain(str);
                return spanned_string;
            });

            return entry.entry.fold_field(
                &field,
                format_matched,
                format_unmatched,
                StyledString::new(),
            );
        }

        Box::new(
            LinearLayout::horizontal()
                .child(
                    TextView::new(create_styled_string(&self, EntryFields::CommandName))
                        .full_width(),
                )
                .child(
                    TextView::new(create_styled_string(&self, EntryFields::CommandStr))
                        .full_width(),
                )
                .child(
                    TextView::new(create_styled_string(&self, EntryFields::Description))
                        .full_width(),
                ),
        )
    }
}

fn main() {
    let args: Args = Args::parse();
    //println!("{:#?}", args);
    let platform = args.platform.unwrap_or(Platform::linux);
    let language = args.language;
    let query = args.query.join(" ");

    let project_dirs: ProjectDirs =
        ProjectDirs::from("com", "DrRuhe", "ttse").expect("Invalid Project Stuff");
    let tmp = Entries::get_instance(project_dirs.cache_dir(), &(language, platform));

    if args.non_interactive {
        let skim = SkimMatcherV2::default();
        let results: Vec<MatchedEntry> = skim
            .fuzzy_search_iter(query, tmp.entries)
            .into_iter()
            .take(1)
            .map(MatchedEntry::from)
            .collect();

        for result in results {
            println!("{}", result);
            println!("{:#?}", result);
        }
    } else {
        run_tui(tmp, &query);
    }

    // TODO Cursive vorgehen: copy from tui-test project.
    // - init layout with search query
    // - edit field has on change callback set to search
    //      - look if async works there
    //      - incremental search? kinda dont want to re-search every time
    // - Select View or List as the LIst of options
    // - find out how to best do the
    // -
    // -
    // -
}

fn run_tui(entries: Entries, query: &str) {
    fn update_list_view(list: &mut ListView, entries: Entries, query: &str) {
        let skim = SkimMatcherV2::default();
        // let fuse = Fuse::default();
        let results: Vec<MatchedEntry> = skim
            .fuzzy_search_iter(query, entries.entries.clone())
            .into_iter()
            .take(100)
            .take_while(|result| result.score > 0f64 || query.len() == 0)
            .map(MatchedEntry::from)
            .collect();

        list.clear();

        for entry in results {
            list.add_child("", entry);
            //view.add_item(format!("{}", c), Content { name: c.into() })
        }
    }

    fn on_edit(s: &mut Cursive, query: &str, _: usize) {
        let query = query.trim();

        if let Some(entries) = s.take_user_data::<Entries>() {
            s.set_user_data(entries.clone());
            s.call_on_name("list", |list: &mut ListView| {
                update_list_view(list, entries, query);
            })
            .expect("could not find list.");
        }
    }

    let mut siv = cursive::default();

    let theme = cursive::theme::Theme {
        borders: BorderStyle::None,
        shadow: false,
        palette: Palette::default(),
    };
    siv.set_theme(theme);

    siv.set_user_data(entries.clone());
    let list = ListView::new()
        .with(|list: &mut ListView| {
            update_list_view(list, entries, query);
        })
        .with_name("list")
        .full_screen()
        .scrollable()
        .scroll_strategy(cursive::view::ScrollStrategy::StickToTop);

    let prompt = EditView::new()
        .content(query)
        .on_edit(on_edit)
        .with_name("prompt");

    siv.add_layer(
        LinearLayout::vertical()
            .child(list)
            .child(prompt)
            .full_screen(),
    );

    siv.focus_name("prompt");
    siv.run();
}
