use std::{
    fmt::Debug,
    fs::{self, read},
    path::{Path, PathBuf},
};

use directories::ProjectDirs;
use fuzzy_searchers::framework::{FuzzySearcheable, WeigtedField};
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use tldr_parsing::{
    acquisition::TLDR,
    structs::{Language, Platform, TLDRPages},
};

pub trait Cached<Info>
where
    Self: Sized,
    Self: Serialize,
    Self: DeserializeOwned,
{
    fn get_instance<P>(directory: P, info: &Info) -> Self
    where
        P: AsRef<Path>,
        P: Debug,
    {
        let cachefile_location = Self::get_cachefile_path(&directory, info);
        match read(&cachefile_location) {
            Ok(bytes) => bincode::deserialize(&bytes).expect("Saved data was corrupted!"),
            Err(_) => {
                let tmp = Self::regenerate_instance(&directory, info);
                tmp.save_instance(&cachefile_location);
                tmp
            }
        }
    }

    fn save_instance<P>(&self, directory: P)
    where
        P: AsRef<Path>,
        P: Debug,
    {
        println!("{:?}", directory);
        fs::write(directory, bincode::serialize(self).unwrap())
            .expect(format!("Error writing pages to location").as_str());
    }
    fn regenerate_instance<P>(directory: P, info: &Info) -> Self
    where
        P: AsRef<Path>;

    fn get_cachefile_path<P>(directory: P, info: &Info) -> PathBuf
    where
        P: AsRef<Path>;
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Entries {
    pub entries: Vec<Entry>,
}

impl Cached<(Language, Platform)> for Entries {
    fn regenerate_instance<P>(directory: P, info: &(Language, Platform)) -> Self
    where
        P: AsRef<Path>,
    {
        let (lang, platform) = info;
        TLDR::new(directory).get_pages(lang, platform).into()
    }

    fn get_cachefile_path<P>(directory: P, info: &(Language, Platform)) -> PathBuf
    where
        P: AsRef<Path>,
    {
        let (lang, platform) = info;
        directory
            .as_ref()
            .join(&format!("entries_{}_{}", platform, lang))
    }
}

impl From<TLDRPages> for Entries {
    fn from(pages: TLDRPages) -> Self {
        let lang = pages.lang;
        let platform = pages.platform;

        let mut vec = vec![];
        for command in pages.programs {
            let command_name: &str = command.name.as_str();
            for example in &command.instructions {
                let tmp = example
                    .command
                    .iter()
                    .map(|i| i.to_owned().into())
                    .collect();

                vec.push(Entry::new(
                    command_name.into(),
                    example.description.clone().into(),
                    tmp,
                ));
            }
        }
        Entries { entries: vec }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Entry {
    pub command_name: String,
    pub description: String,
    pub command: Vec<InstructionPart>,
    pub command_str: String,
}

#[derive(Debug, PartialEq, Eq, Hash)]
pub enum EntryFields {
    CommandName,
    Description,
    CommandStr,
}

impl FuzzySearcheable for Entry {
    type Field = EntryFields;
    fn fields() -> Vec<fuzzy_searchers::framework::WeigtedField<EntryFields>> {
        vec![
            WeigtedField {
                value: EntryFields::CommandName,
                weight: 1f64,
            },
            WeigtedField {
                value: EntryFields::Description,
                weight: 1f64,
            },
            WeigtedField {
                value: EntryFields::CommandStr,
                weight: 1f64,
            },
        ]
    }

    fn lookup(&self, field: &EntryFields) -> std::option::Option<&str> {
        match field {
            EntryFields::CommandName => Some(self.command_name.as_str()),
            EntryFields::Description => Some(self.description.as_str()),
            EntryFields::CommandStr => Some(self.command_str.as_str()),
        }
    }
}

impl Entry {
    fn new(command_name: String, description: String, command: Vec<InstructionPart>) -> Self {
        let command_str = command
            .iter()
            .map(ToString::to_string)
            .collect::<Vec<String>>()
            .join("");

        Entry {
            command_name,
            description,
            command,
            command_str,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum InstructionPart {
    Command(String),
    Placeholder(String),
}

impl From<tldr_parsing::structs::InstructionPart> for InstructionPart {
    fn from(instruction_part: tldr_parsing::structs::InstructionPart) -> Self {
        match instruction_part {
            tldr_parsing::structs::InstructionPart::Command(x) => InstructionPart::Command(x),
            tldr_parsing::structs::InstructionPart::Placeholder(x) => {
                InstructionPart::Placeholder(x)
            }
        }
    }
}

impl ToString for InstructionPart {
    fn to_string(&self) -> String {
        match self {
            InstructionPart::Command(s) => s.to_owned(),
            InstructionPart::Placeholder(s) => s.to_owned(),
        }
    }
}
