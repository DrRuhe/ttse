
# TTSE: The Terminal Search Engine

Have you wanted to do something in the terminal, but did not know what command does that?
Wouldn't a search engine for CLI commands be great?

[![asciicast](https://asciinema.org/a/X9h8HKEModPJ3Dr8iBNIJ8JBg.svg)](https://asciinema.org/a/X9h8HKEModPJ3Dr8iBNIJ8JBg)

`ttse` searches through the examples in the [tldr](https://github.com/tldr-pages/tldr) to provide useful information.

## Installation
Currently, the only way to obtain `ttse` is through compiling from source. For that you will need  installed on your System.

- install [rust](https://doc.rust-lang.org/book/ch01-01-installation.html).
  - `.cargo/bin/` should be in your `$PATH`
  - `rustup` should be availiable on your system
- [Ensure a nightly toolchain is installed:](https://rust-lang.github.io/rustup/concepts/channels.html#working-with-nightly-rust)
```
~$ rustup toolchain install nightly
```
- Install ttse:
```
~$ cargo +nightly install --git https://gitlab.com/DrRuhe/ttse ttse
```

## The Searching Algorithm
Currently I use the [SkimV2 algorithm provided by the fuzzy-matcher](https://docs.rs/crate/fuzzy-matcher/0.3.7) library.

In the future I might attempt to write a custom fuzzy seracher, however this would be a big effort and would probably offer little improvement, if at all.

I might however test out [meilisearch](https://github.com/meilisearch/meilisearch) in the future.

## State

The basic search is done and usable, however to achieve a polished feel, a lot is still left to do:
- [ ] Improve Error reporting and handling
- [ ] let users actually choose platform and language (currently hardcoded to english linux)
- [ ] select a command to be run
  - [ ] make list selectable
  - [ ] customize command using the
- [ ] use a nicer theme instead of blue white
- [ ] publish to crates.io ([How to](https://www.printlnhello.world/blog/publishing-to-crates-io/); maybe use [cargo-release](https://crates.io/crates/cargo-release))

## Ideas
- [x] `--non-interactive` flag, so that users can get search results without promts etc.
- [ ] ony show results for installed programs (list all executables in $PATH.)
- [ ] forward the search query to the browser (using https://github.com/Byron/open-rs)
- [ ] forward search query to cheat.sh
- [ ] integrate functionality of [so](https://github.com/samtay/so)
- [ ] use [meilisearch](https://github.com/meilisearch/meilisearch)
- [ ] allow users to create and edit commands.
## Inspiration
- [navi](https://github.com/denisidoro/navi)
- [cheat.sh](https://cheat.sh) ([repo](https://github.com/chubin/cheat.sh))

## Alternative Names
Inspired by the [QED](https://ftbwiki.org/QED) the Name `ttse` can mean any of the following:
- The TLDR Search Engine
- TTSE totally solves everything
- Terrible Terminal Search Engine
- Terminal Tools Search Engine
- Terrific Terminal Search Engine
