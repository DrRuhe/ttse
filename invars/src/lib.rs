pub struct Invar<'a, A, E> {
    depends_on: Option<Box<dyn InvarTrait<A, E> + 'a>>,
    func: Box<dyn Fn(Option<A>) -> Result<A, E> + 'a>,
}

impl<'a, A, E> Invar<'a, A, E> {
    pub fn new<F>(func: F) -> Self
    where
        F: Fn(Option<A>) -> Result<A, E> + 'a,
    {
        Self {
            depends_on: None,
            func: Box::new(func),
        }
    }
    pub fn then<F>(self, func: F) -> Self
    where
        A: 'a,
        E: 'a,
        F: Fn(Option<A>) -> Result<A, E> + 'a,
    {
        Self {
            depends_on: Some(Box::new(self)),
            func: Box::new(func),
        }
    }
}

pub trait InvarTrait<A, E> {
    fn get_fn(&self) -> &dyn Fn(Option<A>) -> Result<A, E>;
    fn get_dependency(&self) -> Option<&dyn InvarTrait<A, E>>;

    fn execute(&self) -> Result<A, E> {
        let func = self.get_fn();

        match func(None) {
            Ok(worked) => Ok(worked),
            Err(own_fault) => match self.get_dependency() {
                None => Err(own_fault),
                Some(dependency) => match dependency.execute() {
                    Err(err) => Err(err),
                    Ok(incremental_result) => func(Some(incremental_result)),
                },
            },
        }
    }
}

impl<'a, A, E> InvarTrait<A, E> for Invar<'a, A, E> {
    fn get_fn(&self) -> &dyn Fn(Option<A>) -> Result<A, E> {
        self.func.as_ref()
    }

    fn get_dependency(&self) -> Option<&dyn InvarTrait<A, E>> {
        match &self.depends_on {
            None => None,
            Some(x) => Some(x.as_ref()),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn it_works() {
        fn addone(i: Option<u32>) -> Result<u32, u32> {
            match i {
                Some(i) => Ok(i + 1),
                None => Ok(0),
            }
        }

        fn fail(i: Option<u32>) -> Result<u32, u32> {
            match i {
                Some(i) => Ok(i + 1),
                None => Err(0),
            }
        }

        let result = Invar::new(addone)
            .then(addone)
            .then(addone)
            .then(addone)
            .execute();
        assert_eq!(result, Ok(0));

        let result = Invar::new(addone)
            .then(fail)
            .then(fail)
            .then(fail)
            .execute();
        assert_eq!(result, Ok(3));
    }
}
