use cursive::traits::*;
use cursive::view::IntoBoxedView;
use cursive::views::{EditView, LinearLayout, ListView, TextView};
use cursive::Cursive;
use std::process::Command;

#[derive(Debug, Clone)]
struct Content {
    name: String,
}

impl IntoBoxedView for Content {
    fn into_boxed_view(self) -> Box<dyn View> {
        Box::new(
            LinearLayout::horizontal()
                .child(TextView::new("Name:").full_width())
                .child(TextView::new(self.name).full_width()),
        )
    }
}
fn main() {
    let cmd = "tldr echo";

    Command::new("sh")
        .arg("-c")
        .arg(cmd)
        .status()
        .expect("failed to execute process");

    run_tui();
}

fn run_tui() {
    fn on_edit(s: &mut Cursive, content: &str, _: usize) {
        s.call_on_name("list", |view: &mut ListView| {
            view.clear();
            for c in content.chars() {
                view.add_child("test", Content { name: c.into() })
                //view.add_item(format!("{}", c), Content { name: c.into() })
            }
        });
    }
    let mut siv = cursive::default();
    let list = ListView::new()
        .with_name("list")
        .full_screen()
        .scrollable()
        .scroll_strategy(cursive::view::ScrollStrategy::StickToBottom);
    let prompt = EditView::new().on_edit(on_edit).with_name("prompt");
    siv.add_layer(
        LinearLayout::vertical()
            .child(list)
            .child(prompt)
            .full_screen(),
    );
    siv.run();
}
