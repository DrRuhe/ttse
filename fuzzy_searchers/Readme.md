# fuzzy-searchers

a crate providing a framework for and implementations of fuzzy searchers.

## Core Concepts:

- `FuzzySearcheable` Trait: Types implementing this trait can be fuzzy matched/searched.
  - multiple fields with weighted scoring is supported
- `FuzzySearcher` Trait: provide a unified Interface for fuzzy searching across all structs that implement it.


## Examples:

### TODO: Examples: use a fuzzy searcher
### TODO: Examples: define own weights for types