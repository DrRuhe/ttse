use std::{cmp::Ordering, collections::HashMap, fmt::Debug, hash::Hash, ops::Range};

use crate::multirange::{self, MultiRange};

///Trait used to convert types into a query for fuzzy searching.
///Implemented by default on all Types implementing AsRef<str>
pub trait IntoFuzzyQuery {
    /// Produces a string from the type that gets used by FuzzySearchers a Query for searching.
    ///
    /// Default implemented for AsRef<str> Types.
    ///
    /// # Examples
    ///
    /// ```
    /// use fuzzy_searchers::framework::*;
    /// struct Book<'a> {
    ///     name: &'a str,
    ///     author: &'a str,
    /// }
    /// impl<'a> IntoFuzzyQuery for Book<'a> {
    ///     fn into_fuzzy_query(&self) -> &str {
    ///         self.name
    ///     }
    /// }
    ///
    /// fn main(){
    ///     let book = Book{name: "Robinson Crusoe", author: "Daniel Defoe"};
    ///     let query:&str = book.into_fuzzy_query();
    ///     assert_eq!(query,"Robinson Crusoe");
    /// }
    /// ```
    fn into_fuzzy_query(&self) -> &str;
}

/// Trait used to make types fuzzy searcheable.
/// Implementing this trait will make the type usable with methods from the `FuzzySearcher` trait.
///
/// # Examples:
/// ## Make a Struct searcheable:
/// You must now make a decision, with which type to associate the fields in your struct.
/// ### Reccomended Approach: Using an enum to identify fields:
/// ```
/// use fuzzy_searchers::framework::*;
///
/// struct Book<'a> {
///     name: &'a str,
///     description: &'a str,
/// }
/// #[derive(Debug)]
/// enum BookField{
///     Name,
///     Description,
/// }
///
/// impl<'a> FuzzySearcheable<BookField> for Book<'a> {
///     fn fields() -> Vec<WeigtedField<BookField>> {
///         vec![
///             WeigtedField {
///                 value: BookField::Name,
///                 weight: 1.into(),
///             },
///             WeigtedField {
///                 value: BookField::Description,
///                 weight: 1.into(),
///             },
///         ]
///     }
///
///     fn lookup(&self, field: &BookField) -> Option<&str> {
///         match field {
///            BookField::Name => Some(self.name),
///            BookField::Description => Some(self.description),
///        }
///     }
/// }
/// ```
/// ### Using `&str` to identify fields:
/// ```
/// use fuzzy_searchers::framework::*;
///
/// struct Book<'a> {
///     name: &'a str,
///     description: &'a str,
/// }
///
/// impl<'a> FuzzySearcheable<&str> for Book<'a> {
///     fn fields() -> Vec<WeigtedField<&'static str>> {
///         vec![
///             WeigtedField {
///                 value: "name",
///                 weight: 1.into(),
///             },
///             WeigtedField {
///                 value: "description",
///                 weight: 1.into(),
///             },
///         ]
///     }
///
///     fn lookup(&self, field: &&str) -> Option<&str> {
///         match *field {
///             "name" => Some(self.name),
///             "descirption" => Some(self.description),
///             _ => None,
///         }
///     }
/// }
/// ```
/// ### Using `i32` to identify fields:
/// ```
/// use fuzzy_searchers::framework::*;
///
/// struct Book<'a> {
///     name: &'a str,
///     description: &'a str,
/// }
///
/// impl<'a> FuzzySearcheable<i32> for Book<'a> {
///     fn fields() -> Vec<WeigtedField<i32>> {
///         vec![
///             WeigtedField {
///                 value: 1,
///                 weight: 1.into(),
///             },
///             WeigtedField {
///                 value: 2,
///                 weight: 1.into(),
///             },
///         ]
///     }
///
///     fn lookup(&self, field: &i32) -> Option<&str> {
///         match *field {
///             1 => Some(self.name),
///             2 => Some(self.description),
///             _ => None,
///         }
///     }
/// }
/// ```
pub trait FuzzySearcheable {
    type Field: Debug + Hash + Eq;
    /// Provide a list of fields and their weights.
    fn fields() -> Vec<WeigtedField<Self::Field>>;

    /// Looks up a `&str` to be searched given a `field`.
    /// What fields are possible can be defined in the `fields` method.
    fn lookup(&self, field: &Self::Field) -> Option<&str>;

    /// uses `lookup` for every field provided by `fields`
    fn lookup_all(&self) -> Vec<(WeigtedField<Self::Field>, &str)> {
        Self::fields()
            .into_iter()
            .map(|field| {
                let value = self.lookup(&field.value).expect(&format!("lookup/fields implementation faulty: \n Following Field not found by lookup: {:?}",field.value));
                (field, value)
            })
            .collect()
    }
}

/// Use to implemend fuzzy searchers.
/// Provides a unified Interface for fuzzy searching across all structs that implement it.
///
/// # Examples:
/// ## Search for Query: //TODO complete this example once there is a searcher
///```
/// use fuzzy_searchers::framework::*;
///
/// ```
/// ## Implementing own Searcher:
/// Other methods can also be overriden to adjust default behavior for own Searcher:
///  - `calculate_score`
///  - `order_scores`
///
/// Code:
///```
/// use fuzzy_searchers::framework::*;
/// struct TestFuzzySearcher {}
///
/// impl FuzzySearcher for TestFuzzySearcher {
///    fn fuzzy_match(&self, query: &str, haystack: &str) -> (Vec<std::ops::Range<usize>>, f64) {
///        if let Some(start) = haystack.find(&query) {
///            (vec![(start..start + query.len())], 1f64)
///        } else {
///            (vec![], 0f64)
///        }
///    }
/// }
/// ```
///

pub trait FuzzySearcher {
    /// search for `query` in `haystack`
    fn fuzzy_search<Query, Item>(&self, query: Query, haystack: Item) -> FuzzySearchResult<Item>
    where
        Item: FuzzySearcheable,
        Query: IntoFuzzyQuery,
    {
        let query = query.into_fuzzy_query();
        let lookup = haystack.lookup_all();
        let needed_capacity = lookup.len();

        let mut scores = Vec::with_capacity(needed_capacity);
        let mut results = HashMap::with_capacity(needed_capacity);

        for (field, haystack) in lookup {
            let (ranges, score) = self.fuzzy_match(&query, haystack);
            scores.push(WeigtedScore {
                weight: field.weight,
                score,
            });
            results.insert(field.value, FuzzyFieldSearchResult { ranges, score });
        }

        FuzzySearchResult {
            score: self.calculate_score(scores),
            element: haystack,
            results,
        }
    }

    /// search for `query` in an iterator `items`.
    ///
    /// This function takes an owned iterator so it can reference the Items directly and not have to use indices.
    fn fuzzy_search_iter<Query, Items, Item>(
        &self,
        query: Query,
        items: Items,
    ) -> Vec<FuzzySearchResult<Item>>
    where
        Items: IntoIterator<Item = Item>,
        Item: FuzzySearcheable,
        Query: IntoFuzzyQuery,
    {
        let str = query.into_fuzzy_query();
        let mut result: Vec<FuzzySearchResult<Item>> = items
            .into_iter()
            .map(|item| self.fuzzy_search(str, item))
            .collect();
        result.sort_by(|elem, other| Self::order_scores(elem.score, other.score));
        result
    }

    ///## Returns
    /// - `Some(FuzzySearchResult)` : The first element with a better score than threshold
    /// - `None` : There was no element with a better score than threshold
    ///
    ///(Depending on the FuzzySearcher implementation "better score than threshold" can mean different things, Default: `score > threshold`).
    ///
    /// This function takes an owned iterator so it can reference the Items directly and not have to use indices.
    fn fuzzy_search_threshold<Query, Items, Item>(
        &self,
        query: Query,
        items: Items,
        threshold: f64,
    ) -> Option<FuzzySearchResult<Item>>
    where
        Items: IntoIterator<Item = Item>,
        Item: FuzzySearcheable,
        Query: IntoFuzzyQuery,
    {
        let str = query.into_fuzzy_query();
        items.into_iter().find_map(|item| {
            let fuzzy_match = self.fuzzy_search(str, item);
            return if Self::order_scores(fuzzy_match.score, threshold) == Ordering::Greater {
                Some(fuzzy_match)
            } else {
                None
            };
        })
    }
    /// __You probably want `fuzzy_search(query, haystack)` instead!__
    ///
    /// match the query to the haystack and return a range of matches and a score.
    fn fuzzy_match(&self, query: &str, haystack: &str) -> (Vec<Range<usize>>, f64);

    /// __This is a function used internally, so you probably don't want it.__
    ///
    /// Calculates the final score for an item, given the scores of the fields.
    ///
    /// Default calculates a weighted average of all the scores.
    fn calculate_score(&self, results: Vec<WeigtedScore>) -> f64 {
        //TODO: when results is empty it will divide by 0
        let (total, score) = results
            .into_iter()
            .fold((0f64, 0f64), |(total, score), item| {
                let own_score = item.weight * item.score;
                (total + item.weight, score + own_score)
            });

        score / total
    }

    /// Determines how the resulting matches should be ordered.
    fn order_scores(score: f64, other: f64) -> Ordering {
        score.total_cmp(&other)
    }
}

/// A fuzzy searcher that stores information about the query in the struct itself. Therefore
pub trait StatefulFuzzySearcher {
    /// searches in `haystack`
    fn fuzzy_search<Item>(&self, haystack: Item) -> FuzzySearchResult<Item>
    where
        Item: FuzzySearcheable,
    {
        let lookup = haystack.lookup_all();
        let needed_capacity = lookup.len();

        let mut scores = Vec::with_capacity(needed_capacity);
        let mut results = HashMap::with_capacity(needed_capacity);

        for (field, haystack) in lookup {
            let (ranges, score) = self.fuzzy_match(haystack);
            scores.push(WeigtedScore {
                weight: field.weight,
                score,
            });
            results.insert(field.value, FuzzyFieldSearchResult { ranges, score });
        }

        FuzzySearchResult {
            score: self.calculate_score(scores),
            element: haystack,
            results,
        }
    }

    /// searches in an iterator `items`.
    fn fuzzy_search_iter<Items, Item>(&self, items: Items) -> Vec<FuzzySearchResult<Item>>
    where
        Items: IntoIterator<Item = Item>,
        Item: FuzzySearcheable,
    {
        let mut result: Vec<FuzzySearchResult<Item>> = items
            .into_iter()
            .map(|item| self.fuzzy_search(item))
            .collect();
        result.sort_by(|elem, other| Self::order_scores(elem.score, other.score));
        result
    }

    ///## Returns
    /// - `Some(FuzzySearchResult)` : The first element with a better score than threshold
    /// - `None` : There was no element with a better score than threshold
    ///
    ///(Depending on the FuzzySearcher implementation "better score than threshold" can mean different things, Default: `score > threshold`).
    fn fuzzy_search_threshold<Items, Item>(
        &self,
        items: Items,
        threshold: f64,
    ) -> Option<FuzzySearchResult<Item>>
    where
        Items: IntoIterator<Item = Item>,
        Item: FuzzySearcheable,
    {
        items.into_iter().find_map(|item| {
            let fuzzy_match = self.fuzzy_search(item);
            return if Self::order_scores(fuzzy_match.score, threshold) == Ordering::Greater {
                Some(fuzzy_match)
            } else {
                None
            };
        })
    }

    /// __You probably want `fuzzy_search(haystack)` instead!__
    ///
    /// match the query to the haystack and return a range of matches and a score.
    fn fuzzy_match(&self, haystack: &str) -> (Vec<Range<usize>>, f64);

    /// __This is a function used internally, so you probably don't want it.__
    ///
    /// Calculates the final score for an item, given the scores of the fields.
    ///
    /// Default calculates a weighted average of all the scores.
    fn calculate_score(&self, results: Vec<WeigtedScore>) -> f64 {
        //TODO: when results is empty it will divide by 0
        let (total, score) = results
            .into_iter()
            .fold((0f64, 0f64), |(total, score), item| {
                let own_score = item.weight * item.score;
                (total + item.weight, score + own_score)
            });

        score / total
    }

    /// Determines how the resulting matches should be ordered.
    fn order_scores(score: f64, other: f64) -> Ordering {
        score.total_cmp(&other)
    }
}

/// The complete Search result for one `element` produced by searching with the FuzzySearcher trait methods.
#[derive(Debug, PartialEq)]
pub struct FuzzySearchResult<Item>
where
    Item: FuzzySearcheable,
{
    /// The total score of the element when searching with a query.
    pub score: f64,
    /// The element that the result belongs to
    pub element: Item,
    /// Detailed results for each field of the Element, as defined by `element.fields()` and `element.lookup(field)`  methods.
    pub results: HashMap<Item::Field, FuzzyFieldSearchResult>,
}

pub type MatchFormatter = fn(&str, &mut std::fmt::Formatter<'_>) -> std::fmt::Result;
pub type MatchClosure<T> = Box<dyn Fn(&str, T) -> T>;

impl<Item> FuzzySearchResult<Item>
where
    Item: FuzzySearcheable,
{
    /// Can be used to format a field. This function will call the `format_matched` and `format_unmatched` functions with the corresponding parts of the string.
    pub fn format_field(
        &self,
        field: &Item::Field,
        format_matched: MatchFormatter,
        format_unmatched: MatchFormatter,
        f: &mut std::fmt::Formatter<'_>,
    ) -> std::fmt::Result {
        fn process_matched<'a, 'b>(
            format_matched: MatchFormatter,
        ) -> MatchClosure<(&'b mut std::fmt::Formatter<'a>, std::fmt::Result)> {
            return Box::new(move |str, (formatter, result)| {
                let val = match result {
                    Ok(_) => format_matched(str, formatter),
                    Err(x) => Err(x),
                };
                return (formatter, val);
            });
        }

        let (_, val) = self.fold_field(
            field,
            process_matched(format_matched),
            process_matched(format_unmatched),
            (f, Ok(())),
        );

        return val;
        // if let Some(field_result) = self.results.get(field) {
        //     let str = self.element.lookup(field).expect("The field was already looked up before, so it must exist now.");

        //     let multirange = MultiRange::new(0..str.len(), &field_result.ranges);

        //     for (range, selector) in multirange {
        //         match selector {
        //             multirange::RangeSelector::Matched => format_matched(&str[range], f)?,
        //             multirange::RangeSelector::Unmatched => format_unmatched(&str[range], f)?,
        //         }
        //     }
        // }
        // Ok(())
    }

    pub fn fold_field<T>(
        &self,
        field: &Item::Field,
        format_matched: MatchClosure<T>,
        format_unmatched: MatchClosure<T>,
        initial_value: T,
    ) -> T {
        let mut value = initial_value;
        if let Some(field_result) = self.results.get(field) {
            let str = self
                .element
                .lookup(field)
                .expect("The field was already looked up before, so it must exist now.");

            let multirange = MultiRange::new(0..str.len(), &field_result.ranges);

            for (range, selector) in multirange {
                value = match selector {
                    multirange::RangeSelector::Matched => format_matched(&str[range], value),
                    multirange::RangeSelector::Unmatched => format_unmatched(&str[range], value),
                }
            }
        }
        value
    }
}

/// A search result belonging to a single Field of some element.
#[derive(Debug, PartialEq)]
pub struct FuzzyFieldSearchResult {
    /// Ranges where the FuzzySearcher found something fuzzy matching the query.
    pub ranges: Vec<Range<usize>>,
    /// The score for this field.
    pub score: f64,
}

/// Stores information on how the given field is weighted at.
#[derive(Debug, PartialEq)]
pub struct WeigtedField<Field> {
    /// The Field in question.
    pub value: Field,
    /// The weight used in calculating the total score.
    pub weight: f64,
}

/// Stores a score and the associated weight of that score.
#[derive(Debug, PartialEq)]
pub struct WeigtedScore {
    #[allow(missing_docs)]
    pub weight: f64,
    #[allow(missing_docs)]
    pub score: f64,
}

mod standard_impls {
    use super::*;

    impl<T> IntoFuzzyQuery for T
    where
        T: AsRef<str>,
    {
        fn into_fuzzy_query(&self) -> &str {
            self.as_ref()
        }
    }
    impl<T> FuzzySearcheable for T
    where
        T: AsRef<str>,
    {
        type Field = ();

        fn lookup(&self, _: &()) -> Option<&str> {
            Some(self.as_ref())
        }

        fn fields() -> Vec<WeigtedField<()>> {
            vec![WeigtedField {
                value: (),
                weight: 1f64,
            }]
        }
    }
}
