use std::{collections::VecDeque, ops::Range};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum RangeSelector {
    Matched,
    Unmatched,
}

//TODO make this generic
// TODO cleanup
pub struct MultiRange {
    ranges: VecDeque<(usize, Option<RangeSelector>)>,
}

impl MultiRange {
    pub fn new(total: Range<usize>, vec: &Vec<Range<usize>>) -> Self {
        let mut multirange = MultiRange {
            ranges: VecDeque::new(),
        };

        multirange
            .ranges
            .push_back((total.start, Some(RangeSelector::Unmatched)));
        multirange.ranges.push_back((total.end, None));

        for range in vec {
            multirange.add_range(range);
        }
        multirange
    }

    fn add_range(&mut self, range: &Range<usize>) {
        let end = range.end;
        let start = range.start;

        //find out what the end point should have as a range.
        if let Err(index) = self.ranges.binary_search_by_key(&end, |(idx, _)| *idx) {
            //end location not found, insert with rangeselector of previous one,
            let (prev_index, range_selector) = self
                .ranges
                .get(index - 1)
                .expect("There was no previous element.");
            let previous_range_selector = range_selector.clone().expect("test");

            let prev_index = *prev_index;

            self.ranges
                .insert(index, (end, Some(previous_range_selector)));

            // if the previous value is between start and end, remove previous value
            if start <= prev_index && prev_index <= end {
                self.ranges.remove(index - 1);
            }
        }

        //insert start
        match self.ranges.binary_search_by_key(&start, |(idx, _)| *idx) {
            Ok(index) => {
                //start location was found, replace,
                let val = self
                    .ranges
                    .get_mut(index)
                    .expect("idx provided was invalid");
                val.1 = Some(RangeSelector::Matched);
            }
            Err(index) => {
                //start location not found, insert,
                self.ranges
                    .insert(index, (start, Some(RangeSelector::Matched)));
            }
        }

        self.unify()
    }

    fn unify(&mut self) {
        if let Some((_, starting_range_selector)) = self.ranges.front().clone() {
            let mut indices_to_remove = vec![];
            let mut last_selector = starting_range_selector;

            //find all members that do not change the range selector and store their index for removal
            for (idx, (_, curr_selector)) in self.ranges.iter().enumerate() {
                if curr_selector == last_selector && idx != 0 {
                    indices_to_remove.push(idx);
                }
                last_selector = curr_selector;
            }

            for idx in indices_to_remove.iter().rev() {
                self.ranges.remove(*idx);
            }
        }
    }
}

impl Iterator for MultiRange {
    type Item = (Range<usize>, RangeSelector);

    fn next(&mut self) -> Option<Self::Item> {
        while let Some((start, range_selector)) = self.ranges.pop_front() {
            if let Some(range_selector) = range_selector {
                return match self.ranges.front() {
                    Some((end, _)) => Some(((start..*end), range_selector)),
                    None => None, //Some(((start..),range_selector)), //cannot return RangeFrom easily here
                };
            } else {
                // no range selector for that value, try next.
                continue;
            }
        }
        return None;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn multirange_new() {
        let multirange: Vec<(Range<usize>, RangeSelector)> =
            MultiRange::new(0..10, &vec![]).collect();
        assert_eq!(multirange, vec![(0..10, RangeSelector::Unmatched)])
    }
    #[test]
    fn multirange1() {
        let multirange: Vec<(Range<usize>, RangeSelector)> =
            MultiRange::new(0..10, &vec![3..8]).collect();
        assert_eq!(
            multirange,
            vec![
                (0..3, RangeSelector::Unmatched),
                (3..8, RangeSelector::Matched),
                (8..10, RangeSelector::Unmatched)
            ]
        )
    }
    #[test]
    fn multirange_begin_matched() {
        let multirange: Vec<(Range<usize>, RangeSelector)> =
            MultiRange::new(0..10, &vec![0..8]).collect();
        assert_eq!(
            multirange,
            vec![
                (0..8, RangeSelector::Matched),
                (8..10, RangeSelector::Unmatched)
            ]
        )
    }
    #[test]
    fn multirange_end_matched() {
        let multirange: Vec<(Range<usize>, RangeSelector)> =
            MultiRange::new(0..10, &vec![8..10]).collect();
        assert_eq!(
            multirange,
            vec![
                (0..8, RangeSelector::Unmatched),
                (8..10, RangeSelector::Matched)
            ]
        )
    }
    #[test]
    fn multirange_multiple() {
        let multirange: Vec<(Range<usize>, RangeSelector)> =
            MultiRange::new(0..10, &vec![2..4, 6..8]).collect();
        assert_eq!(
            multirange,
            vec![
                (0..2, RangeSelector::Unmatched),
                (2..4, RangeSelector::Matched),
                (4..6, RangeSelector::Unmatched),
                (6..8, RangeSelector::Matched),
                (8..10, RangeSelector::Unmatched),
            ]
        )
    }
    #[test]
    fn multirange_overlap() {
        let multirange: Vec<(Range<usize>, RangeSelector)> =
            MultiRange::new(0..10, &vec![2..6, 4..8]).collect();
        assert_eq!(
            multirange,
            vec![
                (0..2, RangeSelector::Unmatched),
                (2..8, RangeSelector::Matched),
                (8..10, RangeSelector::Unmatched),
            ]
        )
    }
}
