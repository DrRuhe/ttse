//! This crate contains traits and structs to make fuzzy searching as easy and useful as possible.
//!
//! If you want to use the predefined Fuzzy-Searchers, [have a look at the searchers module](crate::searchers).
//! If you want to define your own searchers, [take a look at the `FuzzySearcher` trait](crate::framework::FuzzySearcher)

#![cfg_attr(doc_cfg, feature(doc_cfg))]
#![feature(total_cmp)]
//#![warn(missing_debug_implementations,missing_docs)]

/// Contains the core Traits and Structs provided by this crate.
pub mod framework;

/// All the Different Searchers implemented by this crate.
pub mod searchers;

mod multirange;
use framework::*;

struct TestFuzzySearcher {}

impl FuzzySearcher for TestFuzzySearcher {
    fn fuzzy_match(&self, query: &str, haystack: &str) -> (Vec<std::ops::Range<usize>>, f64) {
        if let Some(start) = haystack.find(&query) {
            (vec![(start..start + query.len())], 1f64)
        } else {
            (vec![], 0f64)
        }
    }
}

struct Book<'a> {
    name: &'a str,
    description: &'a str,
}
#[derive(Debug, PartialEq, Eq, Hash)]
enum BookField {
    Name,
    Description,
}

impl<'a> FuzzySearcheable for Book<'a> {
    type Field = BookField;

    fn fields() -> Vec<WeigtedField<BookField>> {
        vec![
            WeigtedField {
                value: BookField::Name,
                weight: 1.into(),
            },
            WeigtedField {
                value: BookField::Description,
                weight: 1.into(),
            },
        ]
    }

    fn lookup(&self, field: &BookField) -> Option<&str> {
        match field {
            BookField::Name => Some(self.name),
            BookField::Description => Some(self.description),
        }
    }
}

//TODO make traits for automatic display possibilities of matched ranges
//TODO implement some actual fuzzy matchers
//TODO crate features: async, multithreading, async-multithreading
//TODO crate features: each fuzzy matcher shall be disabled b default, users choose which one they want
struct TestStruct {}
impl AsRef<str> for TestStruct {
    fn as_ref(&self) -> &str {
        "test"
    }
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn it_works() {
        let tmp = TestFuzzySearcher {};
        println!(
            "fuzzy_match: {:#?}",
            tmp.fuzzy_match("test", "this is a test")
        );
        let tmp2 = TestStruct {};
        println!(
            "fuzzy_search: {:#?}",
            tmp.fuzzy_search(tmp2, "this is a test")
        );
        println!(
            "fuzzy_search: {:#?}",
            tmp.fuzzy_search("test", "this is a test")
        );
    }
}
