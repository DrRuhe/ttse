#[allow(unused_imports)]
use crate::framework::*;

#[cfg(feature = "fuse")]
#[cfg_attr(doc_cfg, doc(cfg(feature = "fuse")))]
use fuse_rust::Fuse;

#[cfg(feature = "fuse")]
#[cfg_attr(doc_cfg, doc(cfg(feature = "fuse")))]
impl FuzzySearcher for Fuse {
    fn fuzzy_match(&self, query: &str, haystack: &str) -> (Vec<std::ops::Range<usize>>, f64) {
        match self.search_text_in_string(query, haystack) {
            Some(score_result) => (score_result.ranges, score_result.score),
            None => (vec![], 1f64),
        }
    }
}

#[cfg(feature = "ahocorasick")]
#[cfg_attr(doc_cfg, doc(cfg(feature = "ahocorasick")))]
use aho_corasick::AhoCorasick;

#[cfg(feature = "ahocorasick")]
#[cfg_attr(doc_cfg, doc(cfg(feature = "ahocorasick")))]
impl StatefulFuzzySearcher for AhoCorasick {
    fn fuzzy_match(&self, haystack: &str) -> (Vec<std::ops::Range<usize>>, f64) {
        let haystack_len = haystack.chars().count();
        let mut matched_amount: usize = 0;
        let ranges = self
            .find_iter(haystack)
            .map(|location| {
                matched_amount += location.end() - location.start();
                location.start()..location.end()
            })
            .collect();
        let score = ((haystack_len - matched_amount) as f64) / haystack_len as f64;
        (ranges, score)
    }
}

#[cfg(feature = "skim")]
use crate::multirange::MultiRange;
#[cfg(feature = "skim")]
#[cfg_attr(doc_cfg, doc(cfg(feature = "skim")))]
use fuzzy_matcher::{skim::SkimMatcherV2, FuzzyMatcher};
#[cfg(feature = "skim")]
use std::ops::Range;

#[cfg(feature = "skim")]
#[cfg_attr(doc_cfg, doc(cfg(feature = "skim")))]
impl FuzzySearcher for SkimMatcherV2 {
    fn fuzzy_match(&self, query: &str, haystack: &str) -> (Vec<std::ops::Range<usize>>, f64) {
        match self.fuzzy_indices(haystack, query) {
            Some((score, indices)) => {
                let multirange = MultiRange::new(
                    0..haystack.len(),
                    &indices.into_iter().map(|i| (i..i + 1)).collect(),
                );
                let matches: Vec<Range<usize>> = multirange
                    .filter_map(|(range, selector)| match selector {
                        crate::multirange::RangeSelector::Matched => Some(range),
                        crate::multirange::RangeSelector::Unmatched => None,
                    })
                    .collect();
                (matches, score as f64)
            }
            None => (vec![], 0f64),
        }
    }
    fn order_scores(score: f64, other: f64) -> std::cmp::Ordering {
        score.total_cmp(&other).reverse()
    }
}
