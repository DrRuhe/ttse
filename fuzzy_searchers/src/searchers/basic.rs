use crate::framework::FuzzySearcher;

/// Searches for the first full-text match of query.
///
/// The Score is the percentage of unmatched characters
/// (e.g. if 1/3 of characters is matched, there are 2/3 unmatched characters, so the score is 2/3)
///
/// # Examples
/// ```
/// # use std::collections::HashMap;
/// # use fuzzy_searchers::framework::{FuzzyFieldSearchResult, FuzzySearchResult, FuzzySearcher};
/// use fuzzy_searchers::searchers::basic::FindFirstQuery;
///
/// let searcher = FindFirstQuery {};
///
///
///assert_eq!(
///    searcher.fuzzy_search("test", "test"),
///    FuzzySearchResult {
///        score: 0.0, // 0 because it's exact match
///        element: "test",
///        results: HashMap::from([(
///            (),    //the only field for &str is ()
///            FuzzyFieldSearchResult {
///                ranges: vec![0..4,],
///                score: 0.0,
///            }
///        )]),
///    }
///);
///
/// assert_eq!(
///     searcher.fuzzy_search("test", "test2"),
///     FuzzySearchResult {
///         score: 0.2, // 1/5 characters (the 2 at the end) was not matched
///         element: "test2",
///         results: HashMap::from([(
///            (),    //the only field for &str is ()
///            FuzzyFieldSearchResult {
///                ranges: vec![0..4,],
///                score: 0.2,
///            }
///        )]),
///     }
/// );
///
/// assert_eq!(
///     searcher.fuzzy_search("test", "testtest"),
///     FuzzySearchResult {
///         score: 0.5, // only half of characters were matched
///         element: "testtest",
///         results: HashMap::from([(
///            (),    //the only field for &str is ()
///            FuzzyFieldSearchResult {
///                ranges: vec![0..4,],
///                score: 0.5,
///            }
///        )]),
///     }
/// );
/// ```
#[derive(Debug)]
pub struct FindFirstQuery {}

impl FuzzySearcher for FindFirstQuery {
    fn fuzzy_match(&self, query: &str, haystack: &str) -> (Vec<std::ops::Range<usize>>, f64) {
        if let Some(start) = haystack.find(&query) {
            let haystack_len = haystack.chars().count();
            let additional_length: f64 = (haystack_len - query.chars().count()) as f64;
            let score = additional_length / haystack_len as f64;
            (vec![(start..start + query.len())], score)
        } else {
            (vec![], 1f64)
        }
    }
}
