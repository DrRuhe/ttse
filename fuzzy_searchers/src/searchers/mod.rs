/// Implements compatibility for the [fuse-rust crate](https://crates.io/crates/fuse-rust)
pub mod compat;

/// Implements very basic searchers.
pub mod basic;
