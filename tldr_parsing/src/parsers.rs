use std::collections::HashMap;
use std::fs::{read_dir, read_to_string};
use std::io;
use std::path::Path;

use crate::structs::{Instruction, InstructionPart, Language, Platform, Program, TLDRPages};
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::{anychar, newline};
use nom::combinator::{map_opt, peek};
use nom::multi::{many0, many1, many_till, separated_list0};
use nom::sequence::{delimited, tuple};

pub fn parse_tldr_pages<P>(
    path_to_repo: P,
    language: &Language,
    platform: &Platform,
) -> io::Result<TLDRPages>
where
    P: AsRef<Path>,
    P: std::fmt::Debug,
{
    let path = path_to_repo.as_ref().join(language.folder_name());
    let platform_path = path.join(platform.to_string());
    let common_path = path.join("common");

    let common_pages = read_dir(common_path)?.into_iter();
    let platform_pages = read_dir(platform_path)?.into_iter();

    let pages = common_pages.chain(platform_pages);

    let mut programs = HashMap::new();
    for page in pages {
        match page {
            Err(x) => println!("Error reading directory page: {}", x),
            Ok(dir_entry) => {
                if let Some(program) = parse_program(dir_entry.path()) {
                    programs.insert(program.name.clone(), program);
                };
            }
        }
    }

    Ok(TLDRPages {
        lang: *language,
        platform: *platform,
        programs: programs.into_values().collect(),
    })
}

fn parse_program<P>(path: P) -> Option<Program>
where
    P: AsRef<Path>,
    P: std::fmt::Debug,
{
    let file = match read_to_string(&path) {
        Err(x) => {
            println!("Error opening file {:?}:\n{}", path, x);
            return None;
        }
        Ok(file) => file,
    };

    let program = match program(file.as_str()) {
        Err(x) => {
            println!("Error while parsing file {:?}:\n{}", path, x);
            return None;
        }
        Ok((_, program)) => program,
    };
    Some(program)
}

fn program(i: &str) -> nom::IResult<&str, Program> {
    match tuple((
        program_title,
        many0(newline),
        program_description,
        many0(instruction),
    ))(i)
    {
        Err(x) => Err(x),
        Ok((r, (name, _, description, instructions))) => Ok((
            r,
            Program {
                name,
                description,
                instructions,
            },
        )),
    }
}
fn program_title(i: &str) -> nom::IResult<&str, String> {
    match tuple((tag("# "), many_till(anychar, newline)))(i) {
        Err(x) => Err(x),
        Ok((r, (_, (title, _)))) => {
            let title: String = title.into_iter().collect();
            Ok((r, title))
        }
    }
}
fn program_description(i: &str) -> nom::IResult<&str, String> {
    match tuple((
        many0(newline),
        separated_list0(many1(newline), description_line),
    ))(i)
    {
        Err(x) => Err(x),
        Ok((r, (_, lines))) => Ok((r, lines.join("\n"))),
    }
}
fn description_line(i: &str) -> nom::IResult<&str, String> {
    match tuple((tag("> "), many_till(anychar, peek(newline))))(i) {
        Err(x) => Err(x),
        Ok((r, (_, (title, _)))) => {
            let title: String = title.into_iter().collect();
            Ok((r, title))
        }
    }
}
fn instruction(i: &str) -> nom::IResult<&str, Instruction> {
    match tuple((
        many0(newline),
        tag("- "),
        many_till(anychar, tag(":")),
        many0(newline),
        instructionparts,
    ))(i)
    {
        Err(x) => Err(x),
        Ok((r, (_, _, (description, _), _, command))) => {
            let description: String = description.into_iter().collect();
            Ok((
                r,
                Instruction {
                    description,
                    command,
                },
            ))
        }
    }
}
fn instructionparts(i: &str) -> nom::IResult<&str, Vec<InstructionPart>> {
    delimited(tag("`"), many1(instructionpart), tag("`"))(i)
}
fn instructionpart(i: &str) -> nom::IResult<&str, InstructionPart> {
    alt((command, placeholder))(i)
}
fn command(i: &str) -> nom::IResult<&str, InstructionPart> {
    let delimiter = peek(alt((tag("{{"), tag("`"))));
    let (remainder, content) = map_opt(many_till(anychar, delimiter), |(content, _)| {
        if content.len() > 0 {
            return Some(content);
        }
        None
    })(i)?;
    let content: String = content.into_iter().collect();
    Ok((remainder, InstructionPart::Command(content)))
}
fn placeholder(i: &str) -> nom::IResult<&str, InstructionPart> {
    let (remainder, (content, _)) =
        delimited(tag("{{"), many_till(anychar, peek(tag("}}"))), tag("}}"))(i)?;
    let content: String = content.into_iter().collect();
    Ok((remainder, InstructionPart::Placeholder(content)))
}

#[test]
fn test_program() {
    assert_eq!(
                program("# command-name\n\n> Short, snappy description.\n> Preferably one line; two are acceptable if necessary.\n> More information: <https://url-to-upstream.tld>.\n\n- Example description:\n\n`command -opt1 -opt2`\n\n- Example description:\n\n`command -opt1 -opt2 -arg1 {{arg_value}}`"),
                Ok(("",
                Program{
                    name:"command-name".into(),
                    description:"Short, snappy description.\nPreferably one line; two are acceptable if necessary.\nMore information: <https://url-to-upstream.tld>.".into(),
                    instructions:vec![
                        Instruction{
                            description:"Example description".into(),
                            command: vec![InstructionPart::Command("command -opt1 -opt2".into())]
                        },
                        Instruction {
                            description: "Example description".into(),
                            command: vec![
                                InstructionPart::Command("command -opt1 -opt2 -arg1 ".into()),
                                InstructionPart::Placeholder("arg_value".into())
                            ]
                        }
                    ]
                }
            ))
            );
}

#[test]
fn test_program_title() {
    assert_eq!(
        program_title("# command-name\n"),
        Ok(("", "command-name".into()))
    );
}
#[test]
fn test_program_description() {
    assert_eq!(
        program_description("> A\n> B\n> C\n"),
        Ok(("\n", "A\nB\nC".into()))
    );

    assert_eq!(
                program_description("> Short, snappy description.\n> Preferably one line; two are acceptable if necessary.\n> More information: <https://url-to-upstream.tld>.\n"),
                Ok(("\n", "Short, snappy description.\nPreferably one line; two are acceptable if necessary.\nMore information: <https://url-to-upstream.tld>.".into()))
            );
    assert_eq!(
                program_description("> Short, snappy description.\n> Preferably one line; two are acceptable if necessary.\n\n> More information: <https://url-to-upstream.tld>.\n"),
                Ok(("\n", "Short, snappy description.\nPreferably one line; two are acceptable if necessary.\nMore information: <https://url-to-upstream.tld>.".into()))
            );
}
#[test]
fn test_description_line() {
    assert_eq!(
        description_line("> More information: <https://url-to-upstream.tld>.\n"),
        Ok((
            "\n",
            "More information: <https://url-to-upstream.tld>.".into()
        ))
    );
    assert_eq!(
        description_line("> More information: <https://url-to-upstream.tld>.\nABC"),
        Ok((
            "\nABC",
            "More information: <https://url-to-upstream.tld>.".into()
        ))
    );
    assert_eq!(description_line("> \n"), Ok(("\n", "".into())));
}
#[test]
fn test_instruction() {
    assert_eq!(
        instruction("- Example description:\n\n`command -opt1 -opt2 -arg1 {{arg_value}}`"),
        Ok((
            "",
            Instruction {
                description: "Example description".into(),
                command: vec![
                    InstructionPart::Command("command -opt1 -opt2 -arg1 ".into()),
                    InstructionPart::Placeholder("arg_value".into())
                ]
            }
        ))
    );
    assert_eq!(
        instruction("- Example description  :\n\n`command -opt1 -opt2 -arg1 {{arg_value}}`"),
        Ok((
            "",
            Instruction {
                description: "Example description  ".into(),
                command: vec![
                    InstructionPart::Command("command -opt1 -opt2 -arg1 ".into()),
                    InstructionPart::Placeholder("arg_value".into())
                ]
            }
        ))
    );
    assert_eq!(
        instruction("- Example description  \n:\n\n`command -opt1 -opt2 -arg1 {{arg_value}}`"),
        Ok((
            "",
            Instruction {
                description: "Example description  \n".into(),
                command: vec![
                    InstructionPart::Command("command -opt1 -opt2 -arg1 ".into()),
                    InstructionPart::Placeholder("arg_value".into())
                ]
            }
        ))
    );
}
#[test]
fn test_instructionparts() {
    assert_eq!(
        instructionparts("`command -opt1 -opt2 -arg1 {{arg_value}}`"),
        Ok((
            "",
            vec![
                InstructionPart::Command("command -opt1 -opt2 -arg1 ".into()),
                InstructionPart::Placeholder("arg_value".into())
            ]
        ))
    );
    assert_eq!(
        instructionparts("`command -opt1 -opt2 -arg1 {{arg_value}} {{arg_value2}}`"),
        Ok((
            "",
            vec![
                InstructionPart::Command("command -opt1 -opt2 -arg1 ".into()),
                InstructionPart::Placeholder("arg_value".into()),
                InstructionPart::Command(" ".into()),
                InstructionPart::Placeholder("arg_value2".into())
            ]
        ))
    );
    assert_eq!(
        instructionparts("`git -c '{{config.key}}={{wert}}' {{unterbefehl}}`"),
        Ok((
            "",
            vec![
                InstructionPart::Command("git -c '".into()),
                InstructionPart::Placeholder("config.key".into()),
                InstructionPart::Command("=".into()),
                InstructionPart::Placeholder("wert".into()),
                InstructionPart::Command("' ".into()),
                InstructionPart::Placeholder("unterbefehl".into()),
            ]
        ))
    );
}
#[test]
fn test_instructionpart() {
    assert_eq!(
        instructionpart("{{arg_value}}"),
        Ok(("", InstructionPart::Placeholder("arg_value".into())))
    );
    assert_eq!(
        instructionpart("{{argvalue}}"),
        Ok(("", InstructionPart::Placeholder("argvalue".into())))
    );
    assert_eq!(
        instructionpart("{{arg_value}}123"),
        Ok(("123", InstructionPart::Placeholder("arg_value".into())))
    );
    assert_eq!(
        instructionpart("{{arg_value}}}}"),
        Ok(("}}", InstructionPart::Placeholder("arg_value".into())))
    );

    assert_eq!(
        instructionpart("command -opt1 -opt2 -arg1 {{arg_value}}"),
        Ok((
            "{{arg_value}}",
            InstructionPart::Command("command -opt1 -opt2 -arg1 ".into())
        ))
    );
    assert_eq!(
        instructionpart("command -opt1 -opt2 -arg1 {{arg_value}}`"),
        Ok((
            "{{arg_value}}`",
            InstructionPart::Command("command -opt1 -opt2 -arg1 ".into())
        ))
    );
    assert_eq!(
        instructionpart("command -opt1 -opt2 -arg1 `"),
        Ok((
            "`",
            InstructionPart::Command("command -opt1 -opt2 -arg1 ".into())
        ))
    );
    assert_eq!(
        instructionpart("command -opt1 -opt2 -arg1 {} `"),
        Ok((
            "`",
            InstructionPart::Command("command -opt1 -opt2 -arg1 {} ".into())
        ))
    );
}
#[test]
fn test_command() {
    assert_eq!(
        command("command -opt1 -opt2 -arg1 {{arg_value}}"),
        Ok((
            "{{arg_value}}",
            InstructionPart::Command("command -opt1 -opt2 -arg1 ".into())
        ))
    );
    assert_eq!(
        command("command -opt1 -opt2 -arg1 {{arg_value}}`"),
        Ok((
            "{{arg_value}}`",
            InstructionPart::Command("command -opt1 -opt2 -arg1 ".into())
        ))
    );
    assert_eq!(
        command("command -opt1 -opt2 -arg1 `"),
        Ok((
            "`",
            InstructionPart::Command("command -opt1 -opt2 -arg1 ".into())
        ))
    );
    assert_eq!(
        command("command -opt1 -opt2 -arg1 {} `"),
        Ok((
            "`",
            InstructionPart::Command("command -opt1 -opt2 -arg1 {} ".into())
        ))
    );
}
#[test]
fn test_placeholder() {
    assert_eq!(
        placeholder("{{arg_value}}"),
        Ok(("", InstructionPart::Placeholder("arg_value".into())))
    );
    assert_eq!(
        placeholder("{{argvalue}}"),
        Ok(("", InstructionPart::Placeholder("argvalue".into())))
    );
    assert_eq!(
        placeholder("{{arg_value}}123"),
        Ok(("123", InstructionPart::Placeholder("arg_value".into())))
    );
    assert_eq!(
        placeholder("{{arg_value}}}}"),
        Ok(("}}", InstructionPart::Placeholder("arg_value".into())))
    );
}
