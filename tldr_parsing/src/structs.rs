use serde::{Deserialize, Serialize};
use strum_macros::EnumString;

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct TLDRPages {
    pub lang: Language,
    pub platform: Platform,
    pub programs: Vec<Program>,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]

pub struct Program {
    pub name: String,
    pub description: String,
    pub instructions: Vec<Instruction>,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Instruction {
    pub description: String,
    pub command: Vec<InstructionPart>,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum InstructionPart {
    Command(String),
    Placeholder(String),
}

#[allow(non_camel_case_types)]
#[derive(
    Debug, Clone, Copy, PartialEq, Eq, EnumString, strum_macros::Display, Serialize, Deserialize,
)]
pub enum Platform {
    android,
    common,
    linux,
    osx,
    sunos,
    windows,
}

#[allow(non_camel_case_types)]
#[derive(
    Debug, Clone, Copy, PartialEq, Eq, EnumString, strum_macros::Display, Serialize, Deserialize,
)]
pub enum Language {
    en,
    hi,
    it,
    pt_BR,
    ar,
    sh,
    tr,
    ru,
    ml,
    no,
    pt_PT,
    es,
    ja,
    sv,
    fr,
    bs,
    de,
    bn,
    th,
    nl,
    uk,
    ko,
    sr,
    ro,
    id,
    zh,
    zh_TW,
    pl,
    da,
    ne,
    ta,
    fa,
    uz,
}

impl Language {
    pub fn folder_name(&self) -> String {
        let str: &str = match self {
            Language::en => "pages",
            Language::hi => "pages.hi",
            Language::it => "pages.it",
            Language::pt_BR => "pages.pt_BR",
            Language::ar => "pages.ar",
            Language::sh => "pages.sh",
            Language::tr => "pages.tr",
            Language::ru => "pages.ru",
            Language::ml => "pages.ml",
            Language::no => "pages.no",
            Language::pt_PT => "pages.pt_PT",
            Language::es => "pages.es",
            Language::ja => "pages.ja",
            Language::sv => "pages.sv",
            Language::fr => "pages.fr",
            Language::bs => "pages.bs",
            Language::de => "pages.de",
            Language::bn => "pages.bn",
            Language::th => "pages.th",
            Language::nl => "pages.nl",
            Language::uk => "pages.uk",
            Language::ko => "pages.ko",
            Language::sr => "pages.sr",
            Language::ro => "pages.ro",
            Language::id => "pages.id",
            Language::zh => "pages.zh",
            Language::zh_TW => "pages.zh_TW",
            Language::pl => "pages.pl",
            Language::da => "pages.da",
            Language::ne => "pages.ne",
            Language::ta => "pages.ta",
            Language::fa => "pages.fa",
            Language::uz => "pages.uz",
        };
        str.into()
    }
}

impl Default for Language {
    fn default() -> Self {
        Language::en
    }
}
