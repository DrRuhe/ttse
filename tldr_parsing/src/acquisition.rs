//TODO create/find library that conveniently manages this logic of creating folders/download/parsing etc.
use git2::Repository;
use invars::{Invar, InvarTrait};
use std::{
    fs,
    path::{Path, PathBuf},
};

use anyhow::{anyhow, Context, Result};

use crate::{
    parsers::parse_tldr_pages,
    structs::{Language, Platform, TLDRPages},
};

const TLDR_REPO_URL: &str = "https://github.com/tldr-pages/tldr";

pub struct TLDR {
    dir: PathBuf,
}

impl TLDR {
    pub fn new<P>(dirs: P) -> Self
    where
        P: AsRef<Path>,
    {
        TLDR {
            dir: dirs.as_ref().to_owned(),
        }
    }

    pub fn get_pages(&self, lang: &Language, platform: &Platform) -> TLDRPages {
        let folder = self.dir.join("repo");

        fs::create_dir_all(&folder).expect("could not create cache/repo directories");

        // define some closures needed later
        let clear_repository_folder = |_| {
            fs::remove_dir_all(&folder).with_context(|| {
                format!(
                    "Deleting {folder} to prepare for redownloading.",
                    folder = folder.display()
                )
            })?;
            Ok(None)
        };

        let download_repo = |_| {
            println!("Trying to download tldr pages...");
            Repository::clone(TLDR_REPO_URL, &folder).with_context(|| {
                format!(
                    "Cloning the tldr repository from {TLDR_REPO_URL} into {folder}",
                    folder = folder.display()
                )
            })?;
            Ok(None)
        };
        let check_if_repo_is_dirty = |_| {
            //TODO pull changes; see https://github.com/rust-lang/git2-rs/blob/master/examples/pull.rs
            let repo = Repository::open(&folder).with_context(|| {
                format!(
                    "Opening the cached tldr repository at {folder}",
                    folder = folder.display()
                )
            })?;

            let diff = repo.diff_index_to_workdir(None, None).with_context(|| {
                format!(
                    "Retrieving the diff for the git-repo located in {folder} failed.",
                    folder = folder.display()
                )
            })?;
            match diff.get_delta(0).is_none() {
                true => Ok(None),
                false => {
                    Err(anyhow!("Repository is not in a clean state. Please clean up the repository located in {} manually",&folder.display()))
                }
            }
        };
        let parse_pages = |_| {
            println!("Trying to parse tldr pages...");
            let pages = parse_tldr_pages(&folder, lang, platform)?;
            Ok(Some(pages))
        };

        let pages: Result<Option<TLDRPages>> = Invar::new(clear_repository_folder)
            .then(download_repo)
            .then(check_if_repo_is_dirty)
            .then(parse_pages)
            .execute();

        return match pages {
            Ok(Some(pages)) => pages,
            Ok(None) => panic!("Internal option was None unexpectedly. This should never happen."),
            Err(_) => panic!("Error while setting up dependecy"),
        };
    }
}
