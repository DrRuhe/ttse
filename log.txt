wc
Count all lines in a file
wc --lines path/to/file

MatchedEntry {
    entry: FuzzySearchResult {
        score: 0.4985507246376812,
        element: Entry {
            command_name: "wc",
            description: "Count all lines in a file",
            command: [
                Command(
                    "wc --lines ",
                ),
                Placeholder(
                    "path/to/file",
                ),
            ],
            command_str: "wc --lines path/to/file",
        },
        results: {
            CommandName: FuzzyFieldSearchResult {
                ranges: [
                    0..2,
                ],
                score: 0.0,
            },
            Description: FuzzyFieldSearchResult {
                ranges: [
                    10..15,
                ],
                score: 0.8,
            },
            CommandStr: FuzzyFieldSearchResult {
                ranges: [
                    0..2,
                    5..10,
                ],
                score: 0.6956521739130435,
            },
        },
    },
}
wc
Count all lines, words and bytes from `stdin`
find . | wc

MatchedEntry {
    entry: FuzzySearchResult {
        score: 0.5690235690235691,
        element: Entry {
            command_name: "wc",
            description: "Count all lines, words and bytes from `stdin`",
            command: [
                Placeholder(
                    "find .",
                ),
                Command(
                    " | wc",
                ),
            ],
            command_str: "find . | wc",
        },
        results: {
            Description: FuzzyFieldSearchResult {
                ranges: [
                    10..15,
                ],
                score: 0.8888888888888888,
            },
            CommandStr: FuzzyFieldSearchResult {
                ranges: [
                    9..11,
                ],
                score: 0.8181818181818182,
            },
            CommandName: FuzzyFieldSearchResult {
                ranges: [
                    0..2,
                ],
                score: 0.0,
            },
        },
    },
}
wc
Count all characters in a file (taking multi-byte characters into account)
wc --chars path/to/file

MatchedEntry {
    entry: FuzzySearchResult {
        score: 0.6151586368977674,
        element: Entry {
            command_name: "wc",
            description: "Count all characters in a file (taking multi-byte characters into account)",
            command: [
                Command(
                    "wc --chars ",
                ),
                Placeholder(
                    "path/to/file",
                ),
            ],
            command_str: "wc --chars path/to/file",
        },
        results: {
            Description: FuzzyFieldSearchResult {
                ranges: [
                    68..73,
                ],
                score: 0.9324324324324325,
            },
            CommandStr: FuzzyFieldSearchResult {
                ranges: [
                    0..2,
                ],
                score: 0.9130434782608695,
            },
            CommandName: FuzzyFieldSearchResult {
                ranges: [
                    0..2,
                ],
                score: 0.0,
            },
        },
    },
}
git count
Print the total number of commits
git count

MatchedEntry {
    entry: FuzzySearchResult {
        score: 0.6296296296296297,
        element: Entry {
            command_name: "git count",
            description: "Print the total number of commits",
            command: [
                Command(
                    "git count",
                ),
            ],
            command_str: "git count",
        },
        results: {
            Description: FuzzyFieldSearchResult {
                ranges: [],
                score: 1.0,
            },
            CommandStr: FuzzyFieldSearchResult {
                ranges: [
                    4..9,
                ],
                score: 0.4444444444444444,
            },
            CommandName: FuzzyFieldSearchResult {
                ranges: [
                    4..9,
                ],
                score: 0.4444444444444444,
            },
        },
    },
}
wc
Count all words in a file
wc --words path/to/file

MatchedEntry {
    entry: FuzzySearchResult {
        score: 0.6376811594202899,
        element: Entry {
            command_name: "wc",
            description: "Count all words in a file",
            command: [
                Command(
                    "wc --words ",
                ),
                Placeholder(
                    "path/to/file",
                ),
            ],
            command_str: "wc --words path/to/file",
        },
        results: {
            CommandName: FuzzyFieldSearchResult {
                ranges: [
                    0..2,
                ],
                score: 0.0,
            },
            Description: FuzzyFieldSearchResult {
                ranges: [],
                score: 1.0,
            },
            CommandStr: FuzzyFieldSearchResult {
                ranges: [
                    0..2,
                ],
                score: 0.9130434782608695,
            },
        },
    },
}
